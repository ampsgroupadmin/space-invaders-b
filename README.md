- API
  GET /api/highscores [{ playerId: 'player', score: score }]
  GET /api/rooms [{ roomId: 'name', players: ['player1', 'player2'], level: level }]
  POST /api/rooms { roomId: 'name' }

  socket.io 
    /lobby

    - on connect: {
      lobby: {
        rooms: [{ roomId: 'name', players: ['player1', 'player2'], level: level }]
        highscores: [{ playerId: 'player', score: score }]
      }
    }

    - on new room: {
      roomUpdate: { roomId: 'roomId', players: ['player1', 'player2'], level: level }
    }

    - on new highscore: {
      highscoreUpdate: [{ playerId: 'player', score: score }]
    }

    - requesting new room: {
      roomCreate: { roomId: 'roomId' }
    }

  socket.io 
    /game/:roomId/:playerId

    - on connect: {
      gameState {
        ...
      }
    }

