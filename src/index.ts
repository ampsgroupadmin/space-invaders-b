import * as ws from 'ws';
import * as http from 'http';
import * as express from 'express';

import GameManager from './server/GameManager';
import LobbyServer from './server/LobbyServer';
import GameServer from './server/GameServer';

let app = express()

app.use('/', express.static('public'));

let httpServer = http.createServer(app);

let gameManager = new GameManager();

let wssServer = new ws.Server({ server: httpServer })
let lobbyServer = new LobbyServer(wssServer, gameManager);
let gameServer = new GameServer(wssServer, gameManager);

httpServer.on('listening', () => {
  console.log('Web server listening on: 8080');
});

httpServer.on('error', (error: Error) => {
  console.error('Web server error: ' + error.name + ', message: ' + error.message);
});

httpServer.on('close', () => {
  console.log('Closing web server');
});

httpServer.listen(8080, 'localhost');