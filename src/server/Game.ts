import * as events from 'events';

import IGame from './model/IGame'
import IGameState from './model/IGameState'
import IAction from './model/IAction'
import { GAME_CONFIG } from './const'
import Monster from './model/Monster'
import IPlayer from './model/IPlayer'
import IPoint2D from './model/IPoint2D'
import { isHit, handleMonsterHit, handlePlayerHit, playerScore, monsterMove, shotMove, MONSTER_FORM, PLAYER_FORM, updateScore } from './common'

import IGameObservable from './IGameObservable';

class Game implements IGame {
  roomId: string;
  gameState: IGameState;
  actions: { [playerId: string]: IAction }
  canvas: IPoint2D;
  monsterVelocity: number;
  monsterCount: number;
  shotSpeed: number;

  private _emmiter: events.EventEmitter;
  private _gameObservable: IGameObservable;

  constructor(roomId: string, gameObservable: IGameObservable) {
    this.roomId = roomId;
    this.gameState = {
      level: GAME_CONFIG.level,
      monsters: {},
      players: {},
      shots: []
    };
    this.monsterCount = GAME_CONFIG.monsterPerRow * GAME_CONFIG.monsterRows;
    this.monsterVelocity = GAME_CONFIG.monsterVelocity;
    this.shotSpeed = GAME_CONFIG.shotSpeed
    this.canvas = GAME_CONFIG.canvas;

    for (let i = 0; i < GAME_CONFIG.monsterRows; i++) {
      for (let j = 0; j < GAME_CONFIG.monsterPerRow; j++) {
        const monster = new Monster();
        monster.health = 1;
        monster.type = 'normal';
        monster.value = 100;
        monster.position = { x: i * MONSTER_FORM.width / 2, y: 150 + j * MONSTER_FORM.height }
        this.gameState.monsters[monster.monsterId] = monster;
      }
    }

    this._emmiter = new events.EventEmitter();
    this._gameObservable = gameObservable;
    this._emmiter.on('gameUpdate', (gameState: IGameState) => {
      this._gameObservable.onGameUpdate(roomId, this.gameState);
    });
    this._emmiter.on('gameEnd', (playerId: string, score: number) => {
      this._gameObservable.onGameEnd(roomId, playerId, score);
    });
  }

  addPlayer(playerId: string) {
    const player: IPlayer = { playerId, health: 3, score: 0, position: { x: this.canvas.x / 2, y: this.canvas.y - 50 } };
    if (this.gameState.players[playerId])
      throw new Error('Player already exists: ' + playerId);

    this.gameState.players[playerId] = player;

    this._emmiter.emit('gameUpdate', this.gameState);
  }

  removePlayer(playerId: string) {
    if (!this.gameState.players[playerId])
      throw new Error('Player does not exists: ' + playerId);

    const score = this.gameState.players[playerId].score;
    delete this.gameState.players[playerId];

    this._emmiter.emit('gameUpdate', this.gameState);
    this._emmiter.emit('gameEnd', playerId, score);
  }

  start() {
    setInterval(() => {
      this.gameState = this.updateGame();
      this._emmiter.emit('gameUpdate', this.gameState);
    }, 50);

    for (let playerId in this.gameState.players) {
      const player = this.gameState.players[playerId];
      if (player.health === 0) {
        this._emmiter.emit('gameEnd', playerId, this.gameState.players[playerId].score);
      }
    }
  }

  action(playerId: string, action: IAction): void {
    if (!this.actions[playerId]) {
      this.actions[playerId] = action;
    }
  }

  private updateGame() {
    //move monsters
    let newMonsters: { [monsterId: string]: Monster } = Object.keys(this.gameState.monsters).reduce((monsters: { [monsterId: string]: Monster }, monsterId: any) => {
      const monster = monsterMove(this.gameState.monsters[monsterId], this);
      monsters[monster.monsterId] = monster;
      return monsters;
    }, {});
    let newShots = Object.keys(this.gameState.shots).map((shot: any) => {
      return shotMove(shot, this)
    });
    Object.keys(this.gameState.monsters).map((monster: any) => {
      Object.keys(this.gameState.shots).map((shot: any) => {
        if (isHit(shot, monster)) {
          delete newMonsters[monster];
          delete newShots[shot];
        }
      })
    });

    let newPlayers: { [playerId: string]: IPlayer } = Object.keys(this.gameState.players).reduce((players: { [playerId: string]: IPlayer }, playerId: string) => {
      let player = this.gameState.players[playerId];
      Object.keys(this.gameState.shots).map((shot: any) => {
        if (isHit(shot, player)) {
          player = handlePlayerHit(player, shot);
          delete newShots[shot];
        }
      })
      if (player.health !== 0) {
        for (let key in this.actions) {
          if (key === playerId) {
            const action = this.actions[key];
            if (action.shot) {
              newShots.push({position: {x: player.position.x, y: player.position.y - PLAYER_FORM.height/2}, type: 'player', strength: 1, shooterId: playerId, direction: 'UP'});
            }
          }
        }
      }
      players[playerId] = player;
      return players;
    }, {});

    return {
      ...this.gameState,
      monsters: newMonsters,
      shots: newShots,
      players: newPlayers
    }
    //move players

    //move shots

    //check collisions

    //monster shots
    //player shots
  }

}
export default Game;