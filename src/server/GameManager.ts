import * as events from 'events';

import IRoom from './model/IRoom';
import IHighscore from './model/IHighscore';
import IGame from './model/IGame';
import IGameState from './model/IGameState';
import IAction from 'model/IAction';

import Game from './Game';
import ILobbyObservable from './ILobbyObservable';
import IGameObservable from './IGameObservable';

class GameManager implements IGameObservable {
  private _lobbyObservable: ILobbyObservable;
  private _gameObservable: IGameObservable;

  private _emmiter: events.EventEmitter;

  private _rooms: { [roomId: string]: IRoom }
  private _highscores: IHighscore[];

  constructor() {
    this._emmiter = new events.EventEmitter();
    //TODO remove it
    this._rooms = {
      roomOne: { roomId: 'roomOne' }, roomTwo: { roomId: 'roomTwo' }
    };
    //TODO remove it
    this._highscores = [{ playerId: 'one', score: 1000 }, { playerId: 'two', score: 500 }];
  }

  setLobbyObservable(lobbyObservable: ILobbyObservable): void {
    this._lobbyObservable = lobbyObservable;
    this._emmiter.on('roomUpdate', (room: IRoom) => {
      this._lobbyObservable.onRoomUpdate(room);
    });
    this._emmiter.on('highscoresUpdate', (room: IRoom) => {
      this._lobbyObservable.onHighscoresUpdate(this._highscores);
    });
  }

  setGameObservable(gameObservable: IGameObservable): void {
    this._gameObservable = gameObservable;
  }

  getRooms(): IRoom[] {
    return Object.keys(this._rooms).map(roomId => this._rooms[roomId]);
  }

  addRoom(roomId: string): IRoom {
    if (this._rooms[roomId])
      throw new Error('Room already exists');

    const room: IRoom = { roomId };
    this._rooms[roomId] = room;

    this._emmiter.emit('roomUpdate', room);

    return room;
  }

  addPlayer(roomId: string, playerId: string) {
    let room = this._rooms[roomId];
    if (!room) {
      throw new Error('Invalid room');
    }

    if (!room.game) {
      room.game = new Game(roomId, this._gameObservable);
      room.game.addPlayer(playerId);
      room.game.start();
    } else {
      room.game.addPlayer(playerId);
    }

    this._emmiter.emit('roomUpdate', room);

    return room.game.gameState;
  }

  removePlayer(roomId: string, playerId: string) {
    let room = this._rooms[roomId];
    if (!room) {
      throw new Error('Invalid room');
    }

    if (!room.game) {
      throw new Error('No active game');
    }

    room.game.removePlayer(playerId);

    this._emmiter.emit('roomUpdate', room);

    return room.game.gameState;
  }

  action(roomId: string, playerId: string, action: IAction) {
    let room = this._rooms[roomId];
    if (!room) {
      throw new Error('Invalid room');
    }

    if (!room.game) {
      throw new Error('No active game');
    }

    room.game.action(playerId, action);
  }

  getHighscores(): IHighscore[] {
    return this._highscores;
  }

  onGameUpdate(roomId: string, gameState: IGameState): void {
    this._gameObservable.onGameUpdate(roomId, gameState);
  }

  onGameEnd(roomId: string, playerId: string, score: number): void {
    this._highscores.push({playerId, score});
    this._highscores.sort((lhs, rhs) => {
      if (lhs.score === rhs.score) return 0;
      if (lhs.score < rhs.score) return -1;
      if (lhs.score > rhs.score) return 1;
    });

    this._emmiter.emit('highscoresUpdate', this._highscores);

    if (Object.keys(this._rooms[roomId].game.gameState.players).length === 0)
      delete this._rooms[roomId].game;

    this._gameObservable.onGameEnd(roomId, playerId, score);
  }
};

export default GameManager;