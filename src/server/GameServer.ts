import * as http from 'http';
import * as ws from 'ws';
import * as url from 'url';

import IGameState from './model/IGameState';

import GameManager from './GameManager';
import IGameObservable from './IGameObservable';

class GameServer implements IGameObservable {
  private _sockets: { [roomId: string]: { [playerId: string]: ws } };
  private _gameManager: GameManager;

  constructor(wssServer: ws.Server, gameManager: GameManager) {
    this._sockets = {};
    this._gameManager = gameManager;
    this._gameManager.setGameObservable(this);

    wssServer.on('connection', (client: ws) => {
      let parsedUrl = url.parse(client.upgradeReq.url, true);
      let path = parsedUrl.pathname.replace('/', '');
      let args = parsedUrl.query;
      let address = client.upgradeReq.socket.remoteAddress.replace(/([:a-z]+)([0-9\.]+)/, '$2');
      let port = client.upgradeReq.socket.remotePort;

      if (path !== 'game')
        return;

      let roomId = args['roomId'];
      let playerId = args['playerId'];

      if (!roomId || !playerId) {
        console.error('Missing room id or player id');
        client.close(1002, 'Missing room id or player id');
        return;
      }

      console.log('Game connected:', roomId, playerId);
      try {
        const gameState = this._gameManager.addPlayer(roomId, playerId);
        client.send(JSON.stringify({
          gameState
        }));
      } catch (error) {
        client.close(1002, error.message);
      }

      client.on('close', () => {
        console.log('Game closed:', roomId, playerId);
        try {
          this._gameManager.removePlayer(roomId, playerId);
        } catch (error) {
          console.error('Game close error:', roomId, playerId, error);
        }

        delete this._sockets[roomId][playerId];
        if (Object.keys(this._sockets[roomId]).length === 0)
          delete this._sockets[roomId];
      });

      client.on('error', () => {
        console.error('Game error:', roomId, playerId);
        try {
          this._gameManager.removePlayer(roomId, playerId);
        } catch (error) {
          console.error('Game close error:', roomId, playerId, error);
        }

        delete this._sockets[roomId][playerId];
        if (Object.keys(this._sockets[roomId]).length === 0)
          delete this._sockets[roomId];
      });

      client.on('message', (data) => {
        console.log('Game message:', roomId, playerId, data);

        let message: any;
        try {
          message = JSON.parse(data);
        } catch (error) {
          console.error('Game invalid message:', roomId, playerId, error);
        }
        if (message && message.action && message.action.shoot && message.action.move) {
          console.log('Game room create:', roomId, playerId, message.roomCreate.roomId);
          try {
            this._gameManager.action(roomId, playerId, message.action);
          } catch (error) {
            client.send(JSON.stringify({ error: error.message }));
          }
        }
      });

      if (!this._sockets[roomId])
        this._sockets[roomId] = {};

      if (this._sockets[roomId][playerId]) {
        client.close(1002, 'Player already in the room');
        return;
      }
      this._sockets[roomId][playerId] = client;
    });

    wssServer.on('error', (error: Error) => {
      console.error('error', error);
    });
  }

  onGameUpdate(roomId: string, gameState: IGameState): void {
    if (!this._sockets[roomId]) {
      console.error('Game update, invalid room id:', roomId);
      return;
    }

    Object.keys(this._sockets[roomId]).forEach(playerId => {
      this._sockets[roomId][playerId].send(JSON.stringify({ gameState }));
    })
  }

  onGameEnd(roomId: string, playerId: string, score: number): void {
    return;
  }
};

export default GameServer;