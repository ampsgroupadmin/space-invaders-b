import IGameState from './model/IGameState';

interface IGameObservable {
  onGameUpdate(roomId: string, gameState: IGameState): void;
  onGameEnd(roomId: string, playerId: string, score: number): void;
};

export default IGameObservable;