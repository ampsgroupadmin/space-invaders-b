import IRoom from './model/IRoom';
import IHighscore from './model/IHighscore';

interface ILobbyObservable {
  onRoomUpdate(room: IRoom): void;
  onHighscoresUpdate(highscores: IHighscore[]): void;
}

export default ILobbyObservable;