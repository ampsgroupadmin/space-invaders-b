import * as http from 'http';
import * as ws from 'ws';
import * as url from 'url';

import IHighscore from './model/IHighscore';
import IRoom from './model/IRoom';

import GameManager from './GameManager';
import ILobbyObservable from './ILobbyObservable';

class LobbyServer implements ILobbyObservable {
  private _sockets: { [id: string]: ws };
  private _gameManager: GameManager;

  constructor(wssServer: ws.Server, gameManager: GameManager) {
    this._sockets = {};
    this._gameManager = gameManager;
    this._gameManager.setLobbyObservable(this);

    wssServer.on('connection', (client: ws) => {
      let parsedUrl = url.parse(client.upgradeReq.url, true);
      let path = parsedUrl.pathname.replace('/', '');
      let args = parsedUrl.query;
      let address = client.upgradeReq.socket.remoteAddress.replace(/([:a-z]+)([0-9\.]+)/, '$2');
      let port = client.upgradeReq.socket.remotePort;
      let id = address + ':' + port;

      if (path !== 'lobby')
        return;

      client.on('close', () => {
        console.log('Lobby closed:', id);
        delete this._sockets[id];
      });

      client.on('error', () => {
        console.error('Lobby error:', id);
        delete this._sockets[id];
      });

      client.on('message', (data) => {
        console.log('Lobby message:', id, data);

        let message: any;
        try {
          message = JSON.parse(data);
        } catch (error) {
          console.error('Lobby invalid message:', id, error);
        }
        if (message && message.roomCreate && message.roomCreate.roomId) {
          console.log('Lobby room create:', id, message.roomCreate.roomId);
          try {
            this._gameManager.addRoom(message.roomCreate.roomId)
          } catch (error) {
            client.send(JSON.stringify({ error: error.message }));
          }
        }
      });

      console.log('Lobby connected:', id);
      client.send(JSON.stringify({
        lobby: {
          rooms: this._gameManager.getRooms(),
          highscores: this._gameManager.getHighscores()
        }
      }));

      this._sockets[id] = client;
    });

    wssServer.on('error', (error: Error) => {
      console.error('error', error);
    });
  }

  onRoomUpdate(room: IRoom): void {
    console.log('Lobby on room update:', room)
    Object.keys(this._sockets).forEach(id => {
      let roomUpdate: any = { roomId: room.roomId };
      if (room.game) {
        roomUpdate['players'] = Object.keys(room.game.gameState.players);
        roomUpdate['level'] = room.game.gameState.level;
      }
      this._sockets[id].send(JSON.stringify({ roomUpdate }));
    });
  }

  onHighscoresUpdate(highscores: IHighscore[]): void {
    console.log('Lobby on highscores update:', highscores)
    Object.keys(this._sockets).forEach(id => {
      this._sockets[id].send(JSON.stringify({ highscoresUpdate: highscores }));
    });
  }
};

export default LobbyServer;