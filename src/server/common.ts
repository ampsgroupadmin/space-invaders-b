import IGameState from './model/IGameState'
import Monster from './model/Monster'
import IShot from './model/IShot'
import IPlayer from './model/IPlayer'
import IGame from './model/IGame'
import { CANVAS } from './const'

const MONSTER_FORM = {
    width: 83,
    height: 83
};

const PLAYER_FORM = {
    width: 150,
    height: 150
};
  //1014 1677, 83x83, 150x150

const isHit = (shot: IShot, monster: Monster | IPlayer) => {
    if (shot.position.y <= (monster.position.y + (MONSTER_FORM.height / 2))) {
        if (shot.position.x >= (monster.position.x - (MONSTER_FORM.width / 2)) && shot.position.x <= (monster.position.x + (MONSTER_FORM.width / 2)) ){
            return true
        }
    }
};

const handleMonsterHit = (monster: Monster, shot: IShot) => {
    return { ...monster, health: monster.health - shot.strength }
};

const handlePlayerHit = (player: IPlayer, shot: IShot) => {
    return { ...player, health: player.health - shot.strength }
};

const playerScore = (player: IPlayer, monster: Monster) => {
    return { ...player, score: (player.score + monster.value) }
};

const monsterMove = (monster: Monster, game: IGame): Monster => {
    let newX: number;
    let shouldMoveDown: boolean;

    if (monster.moveDirection === 'left') {
        newX = monster.position.x + game.monsterVelocity;
        if (newX <= (CANVAS.x * 0.1)) {
            shouldMoveDown = true
        }
        const newPosition = {
            x: (shouldMoveDown ? monster.position.x : (monster.position.x - game.monsterVelocity)),
            y: (shouldMoveDown ? (monster.position.y + MONSTER_FORM.height + 5)  : monster.position.y)
        };
        return { ...monster, moveDirection: (shouldMoveDown ? 'right' : 'left' ), position: newPosition }
    } else {
        newX = monster.position.x - game.monsterVelocity;
        if (newX >= (CANVAS.x * 0.9)) {
            shouldMoveDown = true
        }
        const newPosition = {
            x: (shouldMoveDown ? monster.position.x : (monster.position.x + game.monsterVelocity)),
            y: ( shouldMoveDown ? (monster.position.y + MONSTER_FORM.height + 5)  : monster.position.y )
        };
        return { ...monster, moveDirection: (shouldMoveDown ? 'right' : 'left' ), position: newPosition }
    }
};

const shotMove = (shot: IShot, game: IGame) => {
    if (shot.direction == 'up') {
        return { ...shot, position: { x: shot.position.x, y: shot.position.y + game.shotSpeed  }}
    }
    return { ...shot, position: { x: shot.position.x, y: shot.position.y - game.shotSpeed  }}
};

const updateScore = (player: IPlayer, monster: Monster) => {
    return { ...player, score: player.score + monster.value }
};


export {
    isHit,
    handleMonsterHit,
    handlePlayerHit,
    playerScore,
    monsterMove,
    shotMove,
    MONSTER_FORM,
    PLAYER_FORM,
    updateScore
}