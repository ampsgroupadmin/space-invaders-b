const CANVAS = {
    x: 100,
    y: 100
};

const GAME_CONFIG = {
    level: 1,
    canvas: CANVAS,
    monsterVelocity: 1,
    monsterPerRow: 10,
    monsterRows: 5,
    shotSpeed: 100
};

export {
    CANVAS,
    GAME_CONFIG
}