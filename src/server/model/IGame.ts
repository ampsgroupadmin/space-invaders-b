import IGameState from 'model/IGameState';
import IAction from 'model/IAction';
import IPoint2D from 'model/IPoint2D';

interface IGame {
  roomId: string;
  gameState: IGameState;
  actions: { [playerId: string]: IAction }
  canvas: IPoint2D;
  monsterVelocity: number;
  monsterCount: number;
  shotSpeed: number;

  addPlayer(playerId: string): void;
  removePlayer(playerId: string): void;
  start(): void;
  action(playerId: string, action: IAction): void;
};

export default IGame;