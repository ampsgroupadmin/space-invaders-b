// const gameState = {
//   config: {
//     level: 1,
//     room: 'roomName'
//   },
//   IIPlayers: {
//     name,
//     life,
//     score,
//     //powerups{}
//   },
//   IMonsters: [
//     {
//       pos: { x: 0, y: 0 },
//       lifes: 0,
//       type: 'type'
//     }
//     //...
//   ],
//   IShots: [
//     {
//       pos: { x: 0, y: 0 },
//       source: 'IMonster|IIPlayer',
//       type: 'IIPlayer_name|IMonster+type'
//     }
//   ]
// }

import Monster from './Monster'
import IPlayer from './IPlayer'
import IShot from './IShot'

interface IGameState {
  level: number;
  monsters: { [monsterId: string]: Monster };
  players: { [playerId: string]: IPlayer };
  shots: IShot[];
}

export default IGameState;
