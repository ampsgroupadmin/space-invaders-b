interface IHighscore {
  playerId: string;
  score: number;
};

export default IHighscore;