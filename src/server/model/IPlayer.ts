import IShot from './IShot'
import IPoint2D from './IPoint2D'

interface IPlayer {
  playerId: string;
  position: IPoint2D;
  health: number;
  score: number
}

export default IPlayer;