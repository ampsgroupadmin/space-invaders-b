import IGame from './IGame';

interface IRoom {
  roomId: string;
  game?: IGame;
};

export default IRoom;