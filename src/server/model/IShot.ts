import IPoint2D from './IPoint2D';

interface IShot {
  position: IPoint2D;
  strength: number;
  type: string;
  shooterId: string;
  direction: string;
}

export default IShot;