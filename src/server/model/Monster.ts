import * as uuid from 'uuid';

import IPoint2D from './IPoint2D'

class Monster {
  monsterId: string;
  position: IPoint2D;
  health: number;
  type: string;
  value: number;
  moveDirection: string;

  constructor() {
    this.monsterId = uuid.v4();
    this.moveDirection = 'left';
  }
}

export default Monster;